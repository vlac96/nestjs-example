# nestjs-project

## Description

First Nest Project

## Installation NestJS

```bash
$ npm install
```

## Running the NestJS

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
npm run start:prod
```
