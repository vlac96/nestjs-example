import { User } from '../users/user.entity';

export class CreatePermissionDto {
    readonly name: string;
    readonly slug: string;
    readonly description: string;
    readonly users: User[];
  }