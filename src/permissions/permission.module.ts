import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PermissionService } from './permission.service';
import { PermissionController } from './permission.controller';
import { Permissions } from './permission.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Permissions])],
  providers: [PermissionService],
  controllers: [PermissionController],
})
export class PermissionModule {}