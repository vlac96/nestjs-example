import { Get, Controller, Render, Body, Post, Param } from '@nestjs/common';
import { PermissionService } from './permission.service';
import { CreatePermissionDto } from './create-permission.dto';

@Controller('permissions')
export class PermissionController {
  constructor(private readonly permissionService: PermissionService) { }

  @Get()
  async findAll() {
    return {permissions: await this.permissionService.findAll()};
  }

  @Post()
  async create(@Body() createPermissionDto: CreatePermissionDto) {
    await this.permissionService.create({ id: null, ...createPermissionDto });
    return 'Create success';
  }

  @Post('update/:id')
  async update(@Body() createPermissionDto: CreatePermissionDto, @Param('id') userId) {
    await this.permissionService.update({ id: userId, ...createPermissionDto });
    return 'Edit success';
  }

  @Post('/delete')
  async delete(@Body() body) {
    await this.permissionService.delete(Number(body.id));
    return 'Delete success';
  }

  @Post('show')
  async show(@Body() id) {
    return { user: await this.permissionService.find(Number(id.id)) };
  }
}
