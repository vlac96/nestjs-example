import { Entity, Column, PrimaryGeneratedColumn, ManyToMany, JoinTable } from 'typeorm';
import { User } from './../users/user.entity';

@Entity('permissions')
export class Permissions {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 50 })
  name: string;

  @Column({ length: 50 })
  slug: string;

  @Column({ length: 255 })
  description: string;

  @ManyToMany(type => User, user => user.permissions)
  users: User[];
}