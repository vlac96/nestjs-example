import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Permissions } from './permission.entity';

@Injectable()
export class PermissionService {
  constructor(
    @InjectRepository(Permissions)
    private readonly permissionRepository: Repository<Permissions>,
  ) { }

  async findAll(): Promise<Permissions[]> {
    return await this.permissionRepository.find({ relations: ['users']});
  }

  async create(permission: Permissions): Promise<any> {
    return await this.permissionRepository.insert(permission);
  }

  async update(permission: Permissions): Promise<any> {
    return await this.permissionRepository.update(permission.id, permission);
  }

  async delete(id: number): Promise<any> {
    return await this.permissionRepository.delete(id);
  }

  async find(id: number): Promise<Permissions> {
    return await this.permissionRepository.findOne(id);
  }
}