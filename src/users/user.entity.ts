import { Entity, Column, PrimaryGeneratedColumn, ManyToMany, JoinTable } from 'typeorm';
import { Permissions } from './../permissions/permission.entity';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 255 })
  firstname: string;

  @Column({ length: 255 })
  lastname: string;

  @Column('int')
  age: number;

  @ManyToMany(type => Permissions, permission => permission.users)
  @JoinTable()
  permissions: Permissions[];
}