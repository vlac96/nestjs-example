import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) { }

  async findAll(query): Promise<[User[], number]> {
    return await this.userRepository.createQueryBuilder('users')
      .leftJoinAndSelect('users.permissions', 'permissions')
      .offset(query.pageNum * query.perPage)
      .limit(query.perPage)
      .getManyAndCount();
  }

  async create(user: User): Promise<any> {
    return await this.userRepository.insert(user);
  }

  async update(user: User): Promise<any> {
    return await this.userRepository.update(user.id, user);
  }

  async delete(id: number): Promise<any> {
    return await this.userRepository.delete(id);
  }

  async find(id: number): Promise<User> {
    return await this.userRepository.findOne(id);
  }

  async findByLastname(lname: string): Promise<number> {
    return await this.userRepository.createQueryBuilder('users')
    .where({lastname: lname})
    .getCount();
  }
}