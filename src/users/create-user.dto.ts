import { Permissions } from '../permissions/permission.entity';
import { IsString, IsInt, Length, Min } from 'class-validator';

export class CreateUserDto {
  @IsString()
  @Length(3, 55)
  readonly firstname: string;

  @IsString()
  @Length(3, 55)
  readonly lastname: string;

  @Min(1)
  @IsInt()
  readonly age: number;

  readonly permissions: Permissions[];
}