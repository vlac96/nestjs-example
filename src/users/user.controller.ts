import { Controller, Body, Post, Param } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './create-user.dto';
import { ValidationPipe } from './../pipe/validation.pipe';

@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) { }

  @Post('/page')
  async findAll(@Body() query) {
    const data = await this.userService.findAll(query);
    return { users: data[0], total: data[1] };
  }

  @Post()
  async create(@Body(new ValidationPipe()) createUserDto: CreateUserDto) {
    await this.userService.create({ id: null, ...createUserDto });
    return 'Create success';
  }

  @Post('update/:id')
  async update(@Body() createUserDto: CreateUserDto, @Param('id') userId) {
    await this.userService.update({ id: userId, ...createUserDto });
    return 'Edit success';
  }

  @Post('/delete')
  async delete(@Body() body) {
    await this.userService.delete(Number(body.id));
    return 'Delete success';
  }

  @Post('show')
  async show(@Body() id) {
    return { user: await this.userService.find(Number(id.id)) };
  }
}
