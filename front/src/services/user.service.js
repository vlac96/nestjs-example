import axios from 'axios';

export const http = {
  async getUsers(query) {
    const response = await axios.post('/users/page', query, {
      'Content-Type': 'application/json'
    });
    return response.data;
  },

  async create(form) {
    const response = await axios.post('/users', form, {
      'Content-Type': 'application/json'
    });
    return response.data;
  },

  async update(id, form) {
    const response = await axios.post('users/update/' + id, form, {
      'Content-Type': 'application/json'
    });
    return response.data;
  },

  async deleteUser(userId) {
    const response = await axios.post('users/delete', {id: userId}, {
      'Content-Type': 'application/json'
    });
    return response.data;
  }

};