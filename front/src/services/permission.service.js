import axios from 'axios';

export const http = {
  async getPermissions() {
    const response = await axios.get('/permissions', {
      'Content-Type': 'application/json'
    });
    return response.data;
  },

  async create(form) {
    const response = await axios.post('/permissions', form, {
      'Content-Type': 'application/json'
    });
    return response.data;
  },

  async update(id, form) {
    const response = await axios.post('permissions/update/' + id, form, {
      'Content-Type': 'application/json'
    });
    return response.data;
  },

  async deleteUser(userId) {
    const response = await axios.post('permissions/delete', {id: userId}, {
      'Content-Type': 'application/json'
    });
    return response.data;
  }

};